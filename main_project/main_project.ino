/******************************************************************************
 * @file    main_project.ino
 * @brief   Главный файл проекта разрядного стенда
 * @version v0.1
 * @date    08.06.2020
 * @author  Власовский Алексей Игоревич & Мануйлов Александр Владимирович
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <SPI.h>
#include <Mcp320x.h>

/*___ Раздел описания переменных проекта ___*/

// все входные данные парсятся в два массива
String input_string = ""; // буфер входного текста
String input_int = ""; // буфер входного числа
uint16_t input_param [2]; //буфер для входных параметров команды
uint8_t param_iterator = 0; //итератор для буфера параметров
uint8_t PC_connection = 0; //Сосотояние, при котором данные передаются на ПК, а не на HMI

//Переменные адреса для сохранения структур
int t_DAC_adress = 0;
int t_ADC_adress = 100;

//Структура для хранения данных о калибровке

//Струкутра канала - координаты двух точек калибровки
struct channel
{
 uint16_t x1;
 uint16_t x2;
 uint16_t y1;
 uint16_t y2; 
};

//Формируем массив структур - восемь каналов АЦП и ЦАП
struct channel tune_DAC[8];
struct channel tune_ADC[8];

//Создадим структуру, описывающую данные на канале разряда
struct cells
{
  uint16_t capacity = 0; //Ёмкость, посчитанная на канале
  uint32_t seconds = 0; //Время разряда в секундах
  uint16_t cutoff_v = 2500; //Напряжение отсечки по умолчанию 2.5 В
  uint16_t state = 0; //Состояние ячейки, по умолчанию выключено
  uint16_t current = 2500; //Ток разряда ячейки, по умолчнию 2.5 А
  uint32_t voltage = 0; //Напряжение на ячейки, обновляется через АЦП
  uint16_t temperature = 0; //Температура ячейки, обновляется через АЦП
  float Rdc = 0; //Эквивалентное внутреннее сопротивление, измеренное по методу DC
  uint16_t It = 2600; //Единица ёмкости ячейки
  uint16_t U1 = 0; //Напряжение при токе 0.2*It
  uint16_t U2 = 0; //Напряжение при токе It
};

//Создаём массив структур - ячеек
struct cells cell[8];

//Дефайны состояний ячейки
#define START 1
#define STOP 0
#define PROCESSING 2
#define WAIT 3
#define TESTING 4

//Массив для хранения 8 команд перезаписи регистров ЦАП
//Одна команда 2 байта, поэтому её необходимо разбить
//на две составляющие
uint8_t DAC_data [2][8];

// В разрядном стенде используются микросхемы
// ЦАП -> AD5328BRUZ
// АЦП -> MCP3208-BI/SL

#define LDAC 4 // Ножка LDAC микросемы ЦАП
#define SYNC 3 // Ножка SYNC микросехмы ЦАП

// Настроим параметры АЦП
#define ADC_CS      2        // SPI slave select
#define VREF    2500     // 2.5V Vref
#define ADC_CLK     1600000  // SPI clock 1.6MHz

// Обьявим АЦП с нашими настройками
MCP3208 adc(VREF, ADC_CS);

// Функция инициализации работы микроконтроллера
void setup()
{

    // читаем данные калибровки ЦАП по адресу 0, указав размер
    eeprom_read_block((void*)&tune_DAC, (void*) t_DAC_adress, sizeof(tune_DAC));
    // читаем данные калибровки АЦП по адресу 100, указав размер
    eeprom_read_block((void*)&tune_ADC, (void*) t_ADC_adress, sizeof(tune_ADC));
      
    //Проинициализируем пины синхронизации ЦАП
    pinMode(LDAC, OUTPUT);
    pinMode(SYNC, OUTPUT);

    //Выставим высокий уровень
    digitalWrite(LDAC, HIGH);
    digitalWrite(SYNC, HIGH);

    //Проинициализируем пин CS АЦП
    pinMode(ADC_CS, OUTPUT);

    //Выставим высокий уровень
    digitalWrite(ADC_CS, HIGH);

    //Настроим UART
    Serial.begin(57600);

    while (!Serial) {
      ; // подождём подключения послетовательного порта
    }

    // На всякий случай установим 1 на выводе Slave Select (D10)
    pinMode(SS, HIGH);
    
    // инициализация Timer1
    cli();  // отключить глобальные прерывания
    TCCR1A = 0;   // установить регистры в 0
    TCCR1B = 0;

    //Пример расчёта значения региста:
    /*
     * (target time) = (timer resolution) * (# timer counts + 1)
     * (# timer counts + 1) = (target time) / (timer resolution)
     * (# timer counts + 1) = (1 s) / (6.4e-5 s)
     * (# timer counts + 1) = 15625
     * (# timer counts) = 15625 - 1 = 15624
     */
    OCR1A = 15624; // установка регистра совпадения
    //Опыт реального измерения показал, что период
    //составляет 1 секунду с точность окло 0.1%

    TCCR1B |= (1 << WGM12);  // включить CTC режим 
    TCCR1B |= (1 << CS10); // Установить биты на коэффициент деления 1024
    TCCR1B |= (1 << CS12);

    TIMSK1 |= (1 << OCIE1A);  // включить прерывание по совпадению таймера
        
    sei(); // включить глобальные прерывания
}

//Функция основного цикла
void loop()
{
  // получение строки и отработка команд
  //пока порт доступен получаем символы
  while (Serial.available() > 0) {
    char inChar = Serial.read();
    
    // если символ конец строки
    if ((inChar == '\n')|(inChar == '}')) {
      
      //Serial.print("Input string: ");
      //Serial.println(input_string);

      //Если пришёл конец строки, то
      //Необходимо проверить было ли число
      //Если было, записать его как параметр
      if (input_int != 0)
      {
        input_param [param_iterator] = input_int.toInt();
        //Сбросим итератор в 0
        param_iterator = 0;
      }

      //Парсинг кода и выполнение необходимых операций
      switch_parse();
      
      // После разбора строки очищаем буферы
      input_string = "";
      input_int = "";
      input_param [0] = 0;
      input_param [1] = 0;
      param_iterator = 0;
    }

    // на каждом считывании символа определяем
    // является ли символ числом
    else if(isDigit(inChar)){
      input_int += inChar;
    }
    // является ли символ пробелом
    else if (inChar == ' ')
    {
      //Если пробел, проверяем состояние итератор
      if ((param_iterator <= 1) & (input_int != 0))
      {
        //записываем input_int в массив параметров
        input_param [param_iterator] = input_int.toInt();
        //Очищаем
        input_int = "";
        //Инкрементируем итератор
        param_iterator ++;
      }
    }
    //Если не пробел и не символ отправляем в строку
    else if (inChar != '}'){
      input_string += inChar;
    }

    //Если приняли много символов
    if ((input_string.length() >= 40) | (input_int.length() >= 40))
    {
      //Очищаем буферы
      input_string = "";
      input_int = "";
      input_param [0] = 0;
      input_param [1] = 0;
      param_iterator = 0;
    }

    //Если символ с кодом 255 или 26 или 224
    if ((inChar == char(255))|(inChar == char(26))|(inChar == char(224)))
    {
      //Очищаем буферы
      input_string = "";
      input_int = "";
      input_param [0] = 0;
      input_param [1] = 0;
      param_iterator = 0;
    }
    
  }
    
}

//Прерывание таймера по сравнению
ISR(TIMER1_COMPA_vect)
{

  //Данное прерывание конфликтует с другими прерываниями, описанными
  //в исходном коде библиотек Arduino.
  //Для того, чтобы данные по прерыванию UART не терялись, внутри этого
  //прерывания так-же необходимо разрешить голобальные прерывания.
  sei(); //включить глобальные прерывания
  
  //Каждое прерывание необходимо обновить данные в ячейках
  //Измеряем темперауру в соответствии с таблицей соответствия
  cell[0].temperature = analogRead(4);
  cell[1].temperature = analogRead(5);
  cell[2].temperature = analogRead(6);
  cell[3].temperature = analogRead(7);
  cell[4].temperature = analogRead(0);
  cell[5].temperature = analogRead(1);
  cell[6].temperature = analogRead(2);
  cell[7].temperature = analogRead(3);

  //Обновляем данные о напряжении, используя калибровку
  ADC_update(0);

  //Пройдёмся по всем ячейкам
  for (int i = 0; i < 8; i++)
  {
    //Если ячейка находится в состоянии старт
    if (cell[i].state == START)
    {
      //Обнуляем данные о времени и ёмкости
      cell[i].seconds = 0;
      cell[i].capacity = 0;

      //генерируем посылку для ЦАП
      parcel_gen(i, cell[i].current, 0);
      //Обновляем регистры ЦАП - выставляем ток
      DAC_update();

      //Переводим ячейку в состояние разряда
      cell[i].state = PROCESSING;
    }

    //Если ячейка находится в состоянии разряда
    if (cell[i].state == PROCESSING)
    {
      //Инкрементируем время на ячейке
      cell[i].seconds ++;
      
      //Проверяем пороговые значения напряжения и температуры
      if ((cell[i].voltage < cell[i].cutoff_v) | (cell[i].temperature < 160))
      {
        //Сбрасываем флаг работы и завершаем разряд
        cell[i].state = STOP;
      }
    }

    //Если состояние стоп
    if (cell[i].state == STOP)
    {
      //Выключаем ток, не используя калибровку
      parcel_gen(i, 0, 1);
      //Обновляем регистры микросхемы ЦАП
      DAC_update();

      //Посчитаем ёмкость в мА*ч
      cell[i].capacity = (int) ((float)(cell[i].current)*(((float)cell[i].seconds)/3600.0));
      //Выставляем ячейки в ждущий режим
      cell[i].state = WAIT;
      cell[i].seconds = 0;
    }

    //Если хотим измерить эквивалентное сопротивление
    if ((cell[i].state == TESTING) && (cell[i].voltage != 0))
    {
      //В начальный этап времени
      if (cell[i].seconds == 0)
      {
        if (PC_connection != 1)
        {
          //Передадим состояние
          Serial.print((String)"t62.txt=\"testing\""+char(255)+char(255)+char(255));
        }
        //Выставляем в канал ток 0.2*It
        parcel_gen(i, (2*cell[i].It)/10, 0);
        //Обновляем регистры микросхемы ЦАП
        DAC_update();
      }

      //На 10-ой секунде
      if (cell[i].seconds == 10)
      {
        cell[i].U1 = cell[i].voltage;
        //Выставляем в канал ток It
        parcel_gen(i, cell[i].It, 0);
        //Обновляем регистры микросхемы ЦАП
        DAC_update();
      }

      //На 11-ой секунде
      if (cell[i].seconds == 11)
      {
        cell[i].U2 = cell[i].voltage;
        cell[i].Rdc = (float) 800*(cell[i].U1 - cell[i].U2)/(cell[i].It);

        if (PC_connection == 1)
        {
          Serial.print("Cell ");
          Serial.print(shuffle(i));
          Serial.print(": ");
          Serial.print(cell[i].Rdc);
          Serial.println(" Ohm");
          Serial.print("U1: ");
          Serial.print(cell[i].U1);
          Serial.println(" mV");
          Serial.print("U2: ");
          Serial.print(cell[i].U2);
          Serial.println(" mV");
          Serial.print("It: ");
          Serial.print(cell[i].It);
          Serial.println(" mA");
        }

        if (PC_connection != 1){
          //Передадим сопротивление в HMI
          Serial.print((String)"t5"+ i +".txt=\""+ cell[i].Rdc +"\""+char(255)+char(255)+char(255));
          //Передадим состояние
          Serial.print((String)"t62.txt=\"done\""+char(255)+char(255)+char(255));
        }
        
        //Выключаем ток, не используя калибровку
        parcel_gen(i, 0, 1);
        //Обновляем регистры микросхемы ЦАП
        DAC_update();
        //Выставляем ячейки в ждущий режим
        cell[i].state = WAIT;
      }

      //После всех проверок по времени засчитываем секунду
      cell[i].seconds++;
      
    }

    //Сбрасываем значение счётчика если ячейка ждёт
    if (cell[i].state == WAIT)
    {
      cell[i].seconds = 0;
    }

    if (PC_connection != 1)
    {
      //отправляем данные на HMI
      //Для начала отправим напряжения
      Serial.print((String)"t32.txt=\""+cell[0].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t33.txt=\""+cell[1].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t34.txt=\""+cell[2].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t35.txt=\""+cell[3].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t36.txt=\""+cell[4].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t37.txt=\""+cell[5].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t38.txt=\""+cell[6].voltage+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t39.txt=\""+cell[7].voltage+"\""+char(255)+char(255)+char(255));
      //Потом отправим ёмкость каждой ячейки
      Serial.print((String)"t25.txt=\""+(cell[0].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t0.txt=\""+(cell[1].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t26.txt=\""+(cell[2].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t27.txt=\""+(cell[3].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t28.txt=\""+(cell[4].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t29.txt=\""+(cell[5].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t30.txt=\""+(cell[6].capacity)+"\""+char(255)+char(255)+char(255));
      Serial.print((String)"t31.txt=\""+(cell[7].capacity)+"\""+char(255)+char(255)+char(255));
    }
    else
    {
      //Каждую секунды выводим основные данные с ячеек
      Serial.print(cell[i].voltage);
      Serial.print(" ");
      Serial.print(cell[i].temperature);
      Serial.print(" ");
    }
  }
  
  if (PC_connection == 1)
  {
    //После отправки строки делаем новую строку
    Serial.println();
  }
}

//Парсер команды
//Методика получения команд основана на соевременном
//разборе входных буферов при получении пробела или конца строки.
//Пробелы при этом игнорируются, поэтому все команды ниже имеют
//соответствующий вид
int parse(String input_string) 
{
  if (input_string.equals(F("setA")) == true) {
    return 10;
  }
  if (input_string.equals(F("setB")) == true) {
    return 11;
  }
  if (input_string.equals(F("setC")) == true) {
    return 12;
  }
  if (input_string.equals(F("setD")) == true) {
    return 13;
  }
  if (input_string.equals(F("setE")) == true) {
    return 14;
  }
  if (input_string.equals(F("setF")) == true) {
    return 15;
  }
  if (input_string.equals(F("setG")) == true) {
    return 16;
  }
  if (input_string.equals(F("setH")) == true) {
    return 17;
  }
  if (input_string.equals(F("setcurrent")) == true) {
    return 18;
  }
  if (input_string.equals(F("pcswitch")) == true) {
    return 19;
  }
  if (input_string.equals(F("DACupdate")) == true) {
    return 20;
  }
  if (input_string.equals(F("DACcheck")) == true) {
    return 21;
  }
  if (input_string.equals(F("ADCread")) == true) {
    return 31;
  }
  if (input_string.equals(F("tempread")) == true) {
    return 41;
  }
  if (input_string.equals(F("t_aDACA")) == true) {
    return 400;
  }
  if (input_string.equals(F("t_bDACA")) == true) {
    return 401;
  }
  if (input_string.equals(F("t_aDACB")) == true) {
    return 410;
  }
  if (input_string.equals(F("t_bDACB")) == true) {
    return 411;
  }
  if (input_string.equals(F("t_aDACC")) == true) {
    return 420;
  }
  if (input_string.equals(F("t_bDACC")) == true) {
    return 421;
  }
  if (input_string.equals(F("t_aDACD")) == true) {
    return 430;
  }
  if (input_string.equals(F("t_bDACD")) == true) {
    return 431;
  }
  if (input_string.equals(F("t_aDACE")) == true) {
    return 440;
  }
  if (input_string.equals(F("t_bDACE")) == true) {
    return 441;
  }
  if (input_string.equals(F("t_aDACF")) == true) {
    return 450;
  }
  if (input_string.equals(F("t_bDACF")) == true) {
    return 451;
  }
  if (input_string.equals(F("t_aDACG")) == true) {
    return 460;
  }
  if (input_string.equals(F("t_bDACG")) == true) {
    return 461;
  }
  if (input_string.equals(F("t_aDACH")) == true) {
    return 470;
  }
  if (input_string.equals(F("t_bDACH")) == true) {
    return 471;
  }
  if (input_string.equals(F("t_aADCA")) == true) {
    return 500;
  }
  if (input_string.equals(F("t_bADCA")) == true) {
    return 501;
  }
  if (input_string.equals(F("t_aADCB")) == true) {
    return 510;
  }
  if (input_string.equals(F("t_bADCB")) == true) {
    return 511;
  }
  if (input_string.equals(F("t_aADCC")) == true) {
    return 520;
  }
  if (input_string.equals(F("t_bADCC")) == true) {
    return 521;
  }
  if (input_string.equals(F("t_aADCD")) == true) {
    return 530;
  }
  if (input_string.equals(F("t_bADCD")) == true) {
    return 531;
  }
  if (input_string.equals(F("t_aADCE")) == true) {
    return 540;
  }
  if (input_string.equals(F("t_bADCE")) == true) {
    return 541;
  }
  if (input_string.equals(F("t_aADCF")) == true) {
    return 550;
  }
  if (input_string.equals(F("t_bADCF")) == true) {
    return 551;
  }
  if (input_string.equals(F("t_aADCG")) == true) {
    return 560;
  }
  if (input_string.equals(F("t_bADCG")) == true) {
    return 561;
  }
  if (input_string.equals(F("t_aADCH")) == true) {
    return 570;
  }
  if (input_string.equals(F("t_bADCH")) == true) {
    return 571;
  }
  if (input_string.equals(F("tDACcheck")) == true) {
    return 499;
  }
  if (input_string.equals(F("tADCcheck")) == true) {
    return 599;
  }
  if (input_string.equals(F("tupdate")) == true) {
    return 300;
  }
  if (input_string.equals(F("start")) == true) {
    return 1;
  }
  if (input_string.equals(F("stop")) == true) {
    return 2;
  }
  if (input_string.equals(F("info")) == true) {
    return 3;
  }
  if (input_string.equals(F("test")) == true) {
    return 4;
  }
  else return 0;
}

//Функция, описывающая поведение кода после
//парсинга команды.
//В данном проекте реализован следующий подход:
//При парсинге входящией строки вычленяются первый и второй параметр команды
//Если параметр пуст, то он равен нулю
//В коде ниже постоянно используется актуальные параметры, вычленные сразу
//После получения символа конца строки
void switch_parse(void)
{
  // используем парсер и получаем ответ
  switch (parse(input_string)) {
    
    case 1:
      //Пройдёмся по всем ячейкам
      for (int i = 0; i < 8; i++)
      {
        cell[i].state = START;
      }
      //Выставим состояние в HMI
      //Serial.print((String)"t42.txt=\"discharge\""+char(255)+char(255)+char(255));
      
      break;

    case 2:
      //Пройдёмся по всем ячейкам
      for (int i = 0; i < 8; i++)
      {
        cell[i].state = STOP;
      }
      //Выставим состояние в HMI
      //Serial.print((String)"t42.txt=\"wait\""+char(255)+char(255)+char(255));
      break;

    case 3:
      //Пройдёмся по всем ячейкам
      for (int i = 0; i < 8; i++)
      {
        Serial.print("Capacity_");
        Serial.print(shuffle(i));
        Serial.print(": ");
        Serial.print(cell[i].capacity);
        Serial.println(" A*h");
      }
      break;

    case 4:
      //Пройдёмся по всем ячейкам
      for (int i = 0; i < 8; i++)
      {
        cell[i].state = TESTING;
      }
      
      if (PC_connection != 1)
      {
      //Выставим состояние в HMI
      Serial.print((String)"t42.txt=\"testing\""+char(255)+char(255)+char(255));
      }
      break;
    
    case 10:
      parcel_gen(0, input_param [0], input_param [1]);
      Serial.println(F("DAC channel A data changed."));
      break;
      
    case 11:
      parcel_gen(1 , input_param [0], input_param [1]);
      Serial.println(F("DAC channel B data changed."));
      break;
  
    case 12:
      parcel_gen(2, input_param [0], input_param [1]);
      Serial.println(F("DAC channel C data changed."));
      break;
  
    case 13:
      parcel_gen(3, input_param [0], input_param [1]);
      Serial.println(F("DAC channel D data changed."));
      break;
  
    case 14:
      parcel_gen(4, input_param [0], input_param [1]);
      Serial.println(F("DAC channel E data changed."));
      break;
      
    case 15:
      parcel_gen(5, input_param [0], input_param [1]);
      Serial.println(F("DAC channel F data changed."));
      break;
  
    case 16:
      parcel_gen(6, input_param [0], input_param [1]);
      Serial.println(F("DAC channel G data changed."));
      break;
  
    case 17:
      parcel_gen(7, input_param [0], input_param [1]);
      Serial.println(F("DAC channel H data changed."));
      break;

    case 18:
      if ((input_param [0] <= 7) && (input_param [0] >= 0))
      {
        cell[input_param [0]].current = input_param [1];
      }
      break;

    case 19:
      PC_connection = input_param[0];
      break;
  
    case 20:       
      DAC_update();     
      Serial.println(F("DAC update complete!"));
      break;
      
    case 21:
      // Выведем массив в терминал
      for (int i = 0; i < 8; i++) {
        Serial.println(DAC_data[0][i]);
        Serial.println(DAC_data[1][i]);
      }
      break;
    
    case 31:
    {
      //Обновляем данные о напряжении на ячайках
      ADC_update(input_param[0]);
      
      // Выведем массив в терминал
      for (int i = 4; i < 8; i++) {
        Serial.print("Slot_");
        Serial.print(shuffle(i));
        Serial.print(": ");
        Serial.print(cell[i].voltage);
        Serial.println(" mV");
      }
      
      // Выведем массив в терминал
      for (int i = 0; i < 4; i++) {
        Serial.print("Slot_");
        Serial.print(shuffle(i));
        Serial.print(": ");
        Serial.print(cell[i].voltage);
        Serial.println(" mV");
      }
  
    }
      break;
  
    case 41:

      //Измеряем темперауру в соответствии с таблицей соответствия
      cell[0].temperature = analogRead(4);
      cell[1].temperature = analogRead(5);
      cell[2].temperature = analogRead(6);
      cell[3].temperature = analogRead(7);
      cell[4].temperature = analogRead(0);
      cell[5].temperature = analogRead(1);
      cell[6].temperature = analogRead(2);
      cell[7].temperature = analogRead(3);
      
      //считаем состояние всех аналоговых входов
      for (int i = 4; i < 8; i++) { 
        Serial.print("temp_");
        Serial.print(shuffle(i));
        Serial.print(" :");
        Serial.println(cell[i].temperature);
      }

      //считаем состояние всех аналоговых входов
      for (int i = 0; i < 4; i++) { 
        Serial.print("temp_");
        Serial.print(shuffle(i));
        Serial.print(" :");
        Serial.println(cell[i].temperature);
      }
      break;

    case 400:
      //Запоминаем точку А калибровкци цап канал А
      tune_DAC[0].x1 = input_param[0];
      tune_DAC[0].y1 = input_param[1];
      break;

    case 401:
      //Запоминаем точку B калибровкци цап канал А
      tune_DAC[0].x2 = input_param[0];
      tune_DAC[0].y2 = input_param[1];
      break;

    case 410:
      //Запоминаем точку А калибровкци цап канал B
      tune_DAC[1].x1 = input_param[0];
      tune_DAC[1].y1 = input_param[1];
      break;

    case 411:
      //Запоминаем точку B калибровкци цап канал B
      tune_DAC[1].x2 = input_param[0];
      tune_DAC[1].y2 = input_param[1];
      break;

    case 420:
      //Запоминаем точку А калибровкци цап канал C
      tune_DAC[2].x1 = input_param[0];
      tune_DAC[2].y1 = input_param[1];
      break;

    case 421:
      //Запоминаем точку B калибровкци цап канал C
      tune_DAC[2].x2 = input_param[0];
      tune_DAC[2].y2 = input_param[1];
      break;

    case 430:
      //Запоминаем точку А калибровкци цап канал D
      tune_DAC[3].x1 = input_param[0];
      tune_DAC[3].y1 = input_param[1];
      break;

    case 431:
      //Запоминаем точку B калибровкци цап канал D
      tune_DAC[3].x2 = input_param[0];
      tune_DAC[3].y2 = input_param[1];
      break;

    case 440:
      //Запоминаем точку А калибровкци цап канал E
      tune_DAC[4].x1 = input_param[0];
      tune_DAC[4].y1 = input_param[1];
      break;

    case 441:
      //Запоминаем точку B калибровкци цап канал E
      tune_DAC[4].x2 = input_param[0];
      tune_DAC[4].y2 = input_param[1];
      break;

    case 450:
      //Запоминаем точку А калибровкци цап канал F
      tune_DAC[5].x1 = input_param[0];
      tune_DAC[5].y1 = input_param[1];
      break;

    case 451:
      //Запоминаем точку B калибровкци цап канал F
      tune_DAC[5].x2 = input_param[0];
      tune_DAC[5].y2 = input_param[1];
      break;

    case 460:
      //Запоминаем точку А калибровкци цап канал G
      tune_DAC[6].x1 = input_param[0];
      tune_DAC[6].y1 = input_param[1];
      break;

    case 461:
      //Запоминаем точку B калибровкци цап канал G
      tune_DAC[6].x2 = input_param[0];
      tune_DAC[6].y2 = input_param[1];
      break;

    case 470:
      //Запоминаем точку А калибровкци цап канал H
      tune_DAC[7].x1 = input_param[0];
      tune_DAC[7].y1 = input_param[1];
      break;

    case 471:
      //Запоминаем точку B калибровкци цап канал H
      tune_DAC[7].x2 = input_param[0];
      tune_DAC[7].y2 = input_param[1];
      break;

    case 500:
      //Запоминаем точку А калибровкци ацп канал А
      tune_ADC[0].x1 = input_param[0];
      tune_ADC[0].y1 = input_param[1];
      break;

    case 501:
      //Запоминаем точку B калибровкци ацп канал А
      tune_ADC[0].x2 = input_param[0];
      tune_ADC[0].y2 = input_param[1];
      break;

    case 510:
      //Запоминаем точку А калибровкци ацп канал B
      tune_ADC[1].x1 = input_param[0];
      tune_ADC[1].y1 = input_param[1];
      break;

    case 511:
      //Запоминаем точку B калибровкци ацп канал B
      tune_ADC[1].x2 = input_param[0];
      tune_ADC[1].y2 = input_param[1];
      break;

    case 520:
      //Запоминаем точку А калибровкци ацп канал C
      tune_ADC[2].x1 = input_param[0];
      tune_ADC[2].y1 = input_param[1];
      break;

    case 521:
      //Запоминаем точку B калибровкци ацп канал C
      tune_ADC[2].x2 = input_param[0];
      tune_ADC[2].y2 = input_param[1];
      break;

    case 530:
      //Запоминаем точку А калибровкци ацп канал D
      tune_ADC[3].x1 = input_param[0];
      tune_ADC[3].y1 = input_param[1];
      break;

    case 531:
      //Запоминаем точку B калибровкци ацп канал D
      tune_ADC[3].x2 = input_param[0];
      tune_ADC[3].y2 = input_param[1];
      break;

    case 540:
      //Запоминаем точку А калибровкци ацп канал E
      tune_ADC[4].x1 = input_param[0];
      tune_ADC[4].y1 = input_param[1];
      break;

    case 541:
      //Запоминаем точку B калибровкци ацп канал E
      tune_ADC[4].x2 = input_param[0];
      tune_ADC[4].y2 = input_param[1];
      break;

    case 550:
      //Запоминаем точку А калибровкци ацп канал F
      tune_ADC[5].x1 = input_param[0];
      tune_ADC[5].y1 = input_param[1];
      break;

    case 551:
      //Запоминаем точку B калибровкци ацп канал F
      tune_ADC[5].x2 = input_param[0];
      tune_ADC[5].y2 = input_param[1];
      break;

    case 560:
      //Запоминаем точку А калибровкци ацп канал G
      tune_ADC[6].x1 = input_param[0];
      tune_ADC[6].y1 = input_param[1];
      break;

    case 561:
      //Запоминаем точку B калибровкци ацп канал G
      tune_ADC[6].x2 = input_param[0];
      tune_ADC[6].y2 = input_param[1];
      break;

    case 570:
      //Запоминаем точку А калибровкци ацп канал H
      tune_ADC[7].x1 = input_param[0];
      tune_ADC[7].y1 = input_param[1];
      break;

    case 571:
      //Запоминаем точку B калибровкци ацп канал H
      tune_ADC[7].x2 = input_param[0];
      tune_ADC[7].y2 = input_param[1];
      break;

    case 499:
      for (int i = 0; i < 8; i++){
        Serial.println(i);
        Serial.println(tune_DAC[i].x1);
        Serial.println(tune_DAC[i].y1);
        Serial.println(tune_DAC[i].x2);
        Serial.println(tune_DAC[i].y2);
      }
      break;

    case 599:
      for (int i = 0; i < 8; i++){
        Serial.println(i);
        Serial.println(tune_ADC[i].x1);
        Serial.println(tune_ADC[i].y1);
        Serial.println(tune_ADC[i].x2);
        Serial.println(tune_ADC[i].y2);
      }
      break;

    case 300:
      // записываем по адресу 0, указав размер
      eeprom_update_block((void*)&tune_DAC, (void*) t_DAC_adress, sizeof(tune_DAC));
      // читаем по адресу 100, указав размер
      eeprom_update_block((void*)&tune_ADC, (void*) t_ADC_adress, sizeof(tune_ADC));
      break;
  
    case 0:
      Serial.println("Invalid String.");
      break;      
  }
}

//Генератор посылок для ЦАП
void parcel_gen(int port, uint32_t voltage, int mode)
{
    int parcel;
    // Составим посылку
    // Список портов
    // А -> 0
    // B -> 1
    // C -> 2
    // D -> 3
    // E -> 4
    // F -> 5
    // G -> 6
    // H -> 7
    // По схеме измеряем Vref в вольтах
    // В коде определено VREF в мВ
    // Формула преобразования y = (4096/Vref)*x, где x - напряжение
    // Адрес идёт перед данными (12 бит), поэтому адрес необходимо сместить
    // Первый бит команды 0, указывает на то что режим работы write

    // В данной версии функции voltage есть уровень сигнала от 0 до 4095
    // Выше приведён метод калибровки для напряжения
    // Однако в данном проекте необходимо клабировать ток, поэтому
    // удобно пользоваться напрямую уровнем сигнала на ЦАП

    //Если появился дополнительный параметр, то мы калибруем и использовать
    //уравнение прямой не нужно
    if (mode == 0)
    {
      //Используем уравнение прямой и данные, полученные при калибровке, чтобы
      //стабилизировать ток равный voltage милиампер
      //Историческая путанница со значением переменной
      voltage = tune_DAC[port].y1 + ((voltage-tune_DAC[port].x1)*\
        (tune_DAC[port].y2-tune_DAC[port].y1))/(tune_DAC[port].x2-tune_DAC[port].x1);

      //задвижка по переполнению
      if (voltage > 4095)
      {
        voltage = 0;
      }
    }

    //Составим команду целиком
    parcel = (port << 12) | (voltage);

    //Распеределим команду на 2 байта и запишем в массив
    DAC_data [0][port] = parcel >> 8;
    DAC_data [1][port] = parcel & 0xFF;
}

//Функция обновления регистров ЦАП через SPI
void DAC_update()
{
  // Инициируем интерфейс SPI
  SPI.begin();


  // Пройдём по всем 8 каналам ЦАП
  for (int i = 0; i < 8; i++) {

    // Начинаем передачу данных, передавая функции объект настроек шины
    // SPISettings( Скорость в Гц, Порядок передачи битов, Режим шины)
    SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE1));

    //Выставим LOW на ноге синхронизации
    digitalWrite(SYNC, LOW);
  
    //Передадим два байта посылки на нужный канал
    SPI.transfer(DAC_data[0][i]);
    SPI.transfer(DAC_data[1][i]);

    // Завершаем передачу данных
    SPI.endTransaction();
    // Подождём 1 мс
    delay(1);
    //Выставим HIGH на ноге синхронизации
    digitalWrite(SYNC, HIGH);
    //Выставим LOW на ноге для обновления рестров ЦАП
    digitalWrite(LDAC, LOW);
    // Подождем 1 мс
    delay(1);
    //Выставим HIGH на ноге для обновления рестров ЦАП
    digitalWrite(LDAC, HIGH);     
  }

  SPI.end();
}

//Функция для обновления данных о напряжении на ячайках
void ADC_update(int param)
{
  // инициализируем SPI для АЦП
  SPISettings settings(ADC_CLK, MSBFIRST, SPI_MODE0);
  SPI.begin();
  SPI.beginTransaction(settings);
  // Получаем данные из АЦП, умножаем их на 2 из за
  // делителя напряжения в схеме и преобразуем их в
  // напряжение согласно Vref
  // Каналы сопрягаются в соответствии с таблицей (ниже)
  cell[0].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_4)*2);
  cell[1].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_5)*2);
  cell[2].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_6)*2);
  cell[3].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_7)*2);
  cell[4].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_0)*2);
  cell[5].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_1)*2);
  cell[6].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_2)*2);
  cell[7].voltage = adc.toAnalog(adc.read(MCP3208::Channel::SINGLE_3)*2);
  SPI.end();
  
  //Если параметра нет, то надо сопоставить данные по калибровке
  if (param == 0)
  {
    for (int i = 0; i < 8; i++){
      cell[i].voltage = tune_ADC[i].x1 + ((cell[i].voltage-tune_ADC[i].y1)*\
        (tune_ADC[i].x2-tune_ADC[i].x1))/(tune_ADC[i].y2-tune_ADC[i].y1);
  
      //Задвижка по переполнению, т.к. работаем с большими числами
      if (cell[i].voltage > 5000)
      {
        cell[i].voltage = 0;
      }
    }
  }
}

//Таблица соответствия каналов для
//экспериментального образца
//Её приходится учитывать при разборе данных
/*_______________________
 * DAC | ADC | temp | GB |
 *  A  |  4  |  4   | 5  |
 *  B  |  5  |  5   | 6  |
 *  C  |  6  |  6   | 7  |
 *  D  |  7  |  7   | 8  |
 *  E  |  0  |  0   | 1  |
 *  F  |  1  |  1   | 3  |
 *  G  |  2  |  2   | 2  | 
 *  H  |  3  |  3   | 4  |
 */

//Перемешивает цифорку слота в соотвествии с таблицей
int shuffle(int i)
{
  switch (i)
  {
    case 0:
      return 5;
      break;

    case 1:
      return 6;
      break;

    case 2:
      return 7;
      break;
      
    case 3:
      return 8;
      break;

    case 4:
      return 1;
      break;

    case 5:
      return 2;
      break;

    case 6:
      return 3;
      break;

    case 7:
      return 4;
      break;
  }
}
